package services.rest;

import com.google.gson.Gson;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.glassfish.jersey.client.ClientConfig;

/**
 *
 * @author Raphael Braganca
 */
public class Departamento {
    
    public static String insertDepartamento(br.unitins.restclient.Departamento departamento){
        ClientConfig config = new ClientConfig();
        Client cliente = ClientBuilder.newClient(config);
        WebTarget target = cliente.target(UriBuilder.
                fromUri("http://172.16.90.96:8080/restserver").build());
        
        Gson gson = new Gson();
        String param = gson.toJson(departamento);
        
//        String departamentos = target.path("rest").path("departamento").path("inserir").
//                request(departamento.getDescricao(), departamento.getIdPai(), departamento.getIdUnidade()).
//                accept(MediaType.APPLICATION_JSON).post(arg0, arg1);
        
        return "";
    }

    public static String getDepartamentosJSON() {
        ClientConfig config = new ClientConfig();
        Client cliente = ClientBuilder.newClient(config);
        WebTarget target = cliente.target(UriBuilder.
                fromUri("http://localhost:8080/restserver").build());

        String departamentos = target.path("rest").path("departamento").path("buscar").
                request().
                accept(MediaType.APPLICATION_JSON).
                get(String.class);

        return (departamentos);
    }

    public static String getDepartamento(Long id) {
        ClientConfig config = new ClientConfig();
        Client cliente = ClientBuilder.newClient(config);
        WebTarget target = cliente.target(UriBuilder.
                fromUri("http://localhost:8080/restserver").build());

        String departamentos = target.path("rest").path("departamento").
                path("buscar").path(id.toString()).
                request().
                accept(MediaType.APPLICATION_JSON).
                get(String.class);

        return (departamentos);
    }

    public static String deleteDepartamento(Integer id) {
        ClientConfig config = new ClientConfig();
        Client cliente = ClientBuilder.newClient(config);
        WebTarget target = cliente.target(UriBuilder.
                fromUri("http://localhost:8080/restserver").build());

        String departamentos = target.path("rest").path("departamento").
                path("excluir").path(id.toString()).
                request().
                accept(MediaType.APPLICATION_JSON).
                delete(String.class);

        return (departamentos);
    }
}
