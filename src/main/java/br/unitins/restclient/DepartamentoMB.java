package br.unitins.restclient;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author raphael
 */
@SessionScoped
@ManagedBean
public class DepartamentoMB {

    Departamento d = new Departamento();
    ArrayList<Departamento> ds = new ArrayList<>();

    public Departamento getD() {
        return d;
    }

    public void setD(Departamento d) {
        this.d = d;
    }

    public ArrayList<Departamento> getDs() {

        return ds;
    }

    public void setDs(ArrayList<Departamento> ds) {
        this.ds = ds;
    }

    public String getDepartamentos() {
        String departamentos = "";
        return departamentos;
    }
    
    public void postDepartamento(){
       // services.rest.Departamento.insertDepartamento();
    }

}
